import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { HeroesServices } from "../../services/heroes.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  heroe:any = [];

  termino:string = "";

  constructor(
    private activatedRoute: ActivatedRoute,
    private _heroesServices: HeroesServices,
    private _router:Router
  ){
    
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params => {

      this.termino = params['nombre'];
      this.heroe = this._heroesServices.buscarHeroe( params['nombre'] );
      console.log( this.heroe );

    })
  }

  verHeroe( idx:number ) {
    console.log(idx);
    this._router.navigate( ['/heroe',idx] );
  }

}
