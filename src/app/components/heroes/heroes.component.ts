import { Component, OnInit } from '@angular/core';
import { HeroesServices, Heroes } from "../../services/heroes.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent {

  heroes: Heroes[] =[];

  constructor(
    private _heroesService: HeroesServices,
    private _router: Router
  ) {
    // console.log('contructor');
    
  }

  ngOnInit(){
  
    this.heroes = this._heroesService.getHeroes();
    // console.log(this.heroes);
    

  }

  /* 
  ?  funcion de redirecionamiento a una pagina con un id se tiene que importar primero el router
  ? despues pasamos la ruta entre []  en el navigate 
  */
  verHeroe( idx:number ) {
    console.log(idx);
    this._router.navigate( ['/heroe',idx] );
  }


}
