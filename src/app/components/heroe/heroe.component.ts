import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { HeroesServices } from "../../services/heroes.service";

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent {

  heroe:any = {};


  constructor( 
    private activatedRoute: ActivatedRoute,
    private _heroesServices: HeroesServices
   ) {
    this.activatedRoute.params.subscribe( params => {
      // console.log( params['id'] );
      this.heroe = this._heroesServices.getHeroe( params['id'] );
      console.log(this.heroe);
      
    });
  }

}
