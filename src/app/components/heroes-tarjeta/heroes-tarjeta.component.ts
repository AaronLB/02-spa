import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from "@angular/router";


@Component({
  selector: 'app-heroes-tarjeta',
  templateUrl: './heroes-tarjeta.component.html',
  styleUrls: ['./heroes-tarjeta.component.css']
})
export class HeroesTarjetaComponent implements OnInit {

  @Input() heroe: any={};
  @Input('index') index: number;

  /* 
   Todo @Output es para escuchar eventos de otro componentet que va de la mano con EventEmitter
  */
  @Output() heroeSeleccionado: EventEmitter<number>;

  constructor( 
    private _router: Router
  ) {
    this.index = 0;
    this.heroeSeleccionado =  new EventEmitter();
  }

  ngOnInit(): void {
    
  }

  verHeroe(){
    console.log(this.index);
    this._router.navigate( ['/heroe',this.index] );
    /* 
    Todo selecionamos la variable previamente usada en @Output y concatenamos emit para emitir un valor o cualquier cosa
    */
    // this.heroeSeleccionado.emit( this.index );
    
  }

}
